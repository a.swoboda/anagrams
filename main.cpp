﻿// anagrams.cpp : Defines the entry point for the application.
//

#include "anagrams.h"

#include <fstream>
#include <iterator>
#include <sstream>

#include <cstdio>
#include <iostream>
#include <iomanip>

#include <unordered_set>
#include <unordered_map>

#include <algorithm>

#include <chrono>

int main(int argc, char *argv[])
{
    if (argc < 2) {
        std::cout << "please provide the path to the dictionary file\n";
        return -1;
    }
	// open words file
	auto const fn = argv[1];
	auto istream = std::ifstream{ fn };
	if (istream.fail()) {
		std::cout << "no words file found\n";
		return 1;
	}

	using Clock = std::chrono::steady_clock;

	auto timeStart = Clock::now();

	auto myHash = [](std::string const& s) { return anagramHash(s); };
	auto myEq = [](std::string lhs, std::string rhs) {
		return areAnagrams(std::move(lhs), std::move(rhs));
	};
	auto anagrams = std::unordered_multiset<std::string,
		decltype(myHash),
		decltype(myEq)>(
			std::istream_iterator<std::string>{istream},
			std::istream_iterator<std::string>{},
			340'000,  // that's the approximate word count
			myHash,
			myEq);

	auto timeHashSetDone = Clock::now();
	auto durationConstruction = timeHashSetDone - timeStart;

	std::cout << "read " << anagrams.size() << " words\n";
	std::cout << "set of size " << anagrams.size() << " took "
		<< std::chrono::duration_cast<std::chrono::milliseconds>(durationConstruction).count()
		<< " milliseconds\n";

    istream.clear();
    istream.seekg(0, std::ios::beg);
    auto map = std::unordered_multimap<std::string, std::string>{340'000};
    auto mapStart = Clock::now();
    for (auto it=std::istream_iterator<std::string>{istream},
        to=std::istream_iterator<std::string>{};
        it != to; ++it) {
        map.emplace(normalizeAnagram(*it), *it);
    }
    auto mapStop = Clock::now();
    auto durMap = mapStop - mapStart;
    std::cout << "map of size " << map.size() << " took " 
        << std::chrono::duration_cast<std::chrono::milliseconds>(durMap).count()
        << " milliseconds\n";


	constexpr auto examples = std::array<char const*, 5>{ 
        "race", "cabinet", "sure", "smile", "arrest" };
	for (auto const& example : examples) {
		std::cout << "anagrams to " << example << ':';
		for (auto fromTo = anagrams.equal_range(example);
			fromTo.first != fromTo.second;
			++fromTo.first) {
			std::cout << ' ' << *fromTo.first;
		}
		std::cout << std::endl;
	}

	return 0;
}
