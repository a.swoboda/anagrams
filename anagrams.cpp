#include "anagrams.h"

#include <algorithm>
#include <numeric>

#include <cctype>
#include <type_traits>  // for std::is_unsigned

#include <array>

bool isPalindrome(std::string const& s) {
	return std::equal(begin(s), end(s), rbegin(s),
		[](int l, int r) { 
			return std::tolower(l) == std::tolower(r); 
		});
}

std::string normalizeAnagram(std::string s) {
	auto it = begin(s);
	for (auto b = cbegin(s), e = cend(s); b != e; ++b) {
		if (std::isalpha(*b)) { *it++ = static_cast<char>(std::tolower(*b)); }
	}
	std::sort(begin(s), it);
	s.erase(it, end(s));
	return std::move(s);
}

namespace {

constexpr std::array<std::size_t, 26> primes = { 
	  2,      3,      5,      7,     11,
	 13,     17,     19,     23,     29,
	 31,     37,     41,     43,     47,
	 53,     59,     61,     67,     71,
	 73,     79,     83,     89,     97,
	101 };

}

std::size_t anagramHash(std::string const& s) {
	static_assert(std::is_unsigned_v<decltype(std::size_t{} * int{})>,
		"On this architecture, the usual arithmetic conversions "
		"between int and std::size_t are signed. "
		"That means this hash implementation has undefined behavior");
	auto accumulator = [](std::size_t acc, auto c) {
		return std::isalpha(c) ? acc * primes[std::tolower(c) - 'a'] : acc;
	};
	return std::accumulate(begin(s), end(s), std::size_t{ 1 }, accumulator);
}

bool areAnagrams(std::string fst, std::string snd) {
	return fst.size() == snd.size()
		&& normalizeAnagram(std::move(fst)) == normalizeAnagram(std::move(snd));
}
