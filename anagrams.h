﻿// anagrams.h : Include file for standard system include files,
// or project specific include files.

#pragma once

#include <string>

bool isPalindrome(std::string const& s);

std::string normalizeAnagram(std::string s);

bool areAnagrams(std::string fst, std::string snd);

std::size_t anagramHash(std::string const& s);
